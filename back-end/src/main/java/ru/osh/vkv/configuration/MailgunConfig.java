package ru.osh.vkv.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "mailgun")
public class MailgunConfig {
    private String apiKey;
    private String url;
}
