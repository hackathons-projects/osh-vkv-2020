package ru.osh.vkv.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.math.BigDecimal;

@Getter
@Setter
@ConfigurationProperties(prefix = "scoring")
public class ScoringConfig {

    private BigDecimal boundary;
}
