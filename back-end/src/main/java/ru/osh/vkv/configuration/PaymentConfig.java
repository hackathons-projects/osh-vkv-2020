package ru.osh.vkv.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "payment")
public class PaymentConfig {

    private String template;

    private String name;

    private String personalAcc;

    private String bankName;

    private String bic;

    private String correspAcc;

    private String kpp;

    private String purpose;

    private String payerAcc;

    private String category;
}
