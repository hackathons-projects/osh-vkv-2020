package ru.osh.vkv.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.MeterEntity;
import ru.osh.vkv.domain.model.SubmissionEntity;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.MeterRepository;
import ru.osh.vkv.domain.repository.SubmissionRepository;
import ru.osh.vkv.domain.repository.UserRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
@Transactional
@Component
public class V4_SubmissionMigration implements Migration {

    private final MeterRepository meterRepository;
    private final UserRepository userRepository;
    private final SubmissionRepository submissionRepository;

    @Override
    public void migrate() {
        Random random = new Random();
        UserEntity testUser = userRepository.findByUsername("user").orElseThrow();
        List<MeterEntity> allByUser = meterRepository.findAllByUser(testUser);
        allByUser.forEach(meterEntity -> {
            submissionRepository.save(new SubmissionEntity(testUser, meterEntity, "Address 1", BigDecimal.ZERO, BigDecimal.TEN, true));
            submissionRepository.save(new SubmissionEntity(testUser, meterEntity, "Address 1", BigDecimal.TEN, BigDecimal.valueOf(10 + random.nextFloat()), false));
        });
    }
}
