package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.osh.vkv.domain.model.SubmissionEntity;
import ru.osh.vkv.domain.model.UserEntity;

import java.util.List;
import java.util.Optional;

public interface SubmissionRepository extends PagingAndSortingRepository<SubmissionEntity, Long>, JpaRepository<SubmissionEntity, Long> {

    Optional<SubmissionEntity> findOneById(Long id);

    List<SubmissionEntity> findAllByUser(UserEntity user);
}
