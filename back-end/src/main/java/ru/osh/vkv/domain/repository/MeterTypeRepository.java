package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.osh.vkv.domain.model.MeterTypeEntity;

public interface MeterTypeRepository extends JpaRepository<MeterTypeEntity, Long> {

    MeterTypeEntity findByName(String name);
}
