package ru.osh.vkv.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "submissions")
public class SubmissionEntity extends BaseEntity<Long> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "meter_id", nullable = false)
    private MeterEntity meter;

    @Column
    private String address;

    @Column
    private BigDecimal previousValue;

    @Column
    private BigDecimal value;

    @Column
    private Boolean payed;
}
