package ru.osh.vkv.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Audited
@Entity
@Table(name = "tariffs")
public class TariffEntity extends BaseEntity<Long> {

    @OneToOne
    @JoinColumn(name = "meter_type_id", referencedColumnName = "id")
    private MeterTypeEntity meterType;

    @Column
    private BigDecimal amountPerOne;
}
