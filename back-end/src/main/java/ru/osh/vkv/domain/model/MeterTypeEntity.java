package ru.osh.vkv.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Audited
@Entity
@Table(name = "meter_types")
public class MeterTypeEntity extends BaseEntity<Long> {

    @Column
    private String name;

    @Column
    private String currency;
}
