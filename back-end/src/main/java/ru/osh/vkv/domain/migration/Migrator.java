package ru.osh.vkv.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.osh.vkv.domain.model.MigrationEntity;
import ru.osh.vkv.domain.repository.MigrationRepository;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Component
public class Migrator {

    private final List<Migration> dataBaseMigrations;
    private final MigrationRepository migrationRepository;

    @PostConstruct
    public void migrate() {
        dataBaseMigrations.stream()
                .filter(migration -> !migrationRepository.existsByClassName(migration.getClass().getName()))
                .sorted(Comparator.comparing(m -> m.getClass().getName()))
                .forEach(migration -> {
                    migration.migrate();
                    migrationRepository.save(new MigrationEntity(migration.getClass().getName()));
                });
    }

}
