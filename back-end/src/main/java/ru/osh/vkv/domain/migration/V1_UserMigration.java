package ru.osh.vkv.domain.migration;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.Role;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.UserRepository;
import ru.osh.vkv.model.request.UserRegistrationRequest;
import ru.osh.vkv.service.UserService;

@RequiredArgsConstructor
@Transactional
@Component
public class V1_UserMigration implements Migration {

    private final UserService userService;
    private final UserRepository userRepository;

    @Override
    public void migrate() {
        UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setUsername("user");
        userRegistrationRequest.setPassword("user");
        userRegistrationRequest.setNickname("user");
        userRegistrationRequest.setFirstName("Тестовый");
        userRegistrationRequest.setSurname("Пользователь");
        userRegistrationRequest.setLastName("Системы");
        userRegistrationRequest.setPhoneNumber("+7-999-999-99-99");
        userService.signUp(userRegistrationRequest);

        userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setUsername("admin");
        userRegistrationRequest.setPassword("admin");
        userRegistrationRequest.setNickname("admin");
        userRegistrationRequest.setPhoneNumber("+7-999-999-99-00");
        userService.signUp(userRegistrationRequest);

        UserEntity admin = userRepository.findByUsername("admin").orElseThrow();
        admin.setRole(Role.ADMIN);
        userRepository.save(admin);
    }
}
