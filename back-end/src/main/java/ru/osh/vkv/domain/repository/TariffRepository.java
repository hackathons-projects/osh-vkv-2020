package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.osh.vkv.domain.model.TariffEntity;

public interface TariffRepository extends JpaRepository<TariffEntity, Long> {
}
