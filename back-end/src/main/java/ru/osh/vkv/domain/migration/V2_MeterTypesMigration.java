package ru.osh.vkv.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.MeterTypeEntity;
import ru.osh.vkv.domain.repository.MeterTypeRepository;

@RequiredArgsConstructor
@Transactional
@Component
public class V2_MeterTypesMigration implements Migration {

    private final MeterTypeRepository meterTypeRepository;

    @Override
    public void migrate() {
        meterTypeRepository.save(new MeterTypeEntity("ELECTRICITY", "KW/h"));
        meterTypeRepository.save(new MeterTypeEntity("WATER", "Cubic meter"));
        meterTypeRepository.save(new MeterTypeEntity("GAS", "Cubic meter"));
        meterTypeRepository.save(new MeterTypeEntity("WARM", "GCal"));
        meterTypeRepository.save(new MeterTypeEntity("AIR", "Cubic meter"));
        meterTypeRepository.save(new MeterTypeEntity("DISTANCE", "Km"));
        meterTypeRepository.save(new MeterTypeEntity("LIFE", "Years"));
    }
}
