package ru.osh.vkv.domain.model;

import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"submissions"})
@ToString(exclude = {"submissions"})
@Audited
@Entity
@Table(name = "meters")
public class MeterEntity extends BaseEntity<Long> {

    @NotAudited
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @NotAudited
    @OneToMany(mappedBy = "meter", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SubmissionEntity> submissions = new HashSet<>();

    @Column(nullable = false)
    private String identifier;

    @OneToOne
    @JoinColumn(name = "meter_type_id", referencedColumnName = "id")
    private MeterTypeEntity meterType;

    @Column
    private String currentAddress;

    @Column
    private BigDecimal currentValue = BigDecimal.ZERO;

    @Column
    private Boolean isEnabled;

}
