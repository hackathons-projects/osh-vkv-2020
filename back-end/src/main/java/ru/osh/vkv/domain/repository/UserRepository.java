package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.osh.vkv.domain.model.UserEntity;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByUsername(String username);

    Boolean existsByUsername(String username);

    void deleteByUsername(String username);
}
