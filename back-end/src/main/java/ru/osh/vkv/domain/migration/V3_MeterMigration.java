package ru.osh.vkv.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.MeterEntity;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.MeterRepository;
import ru.osh.vkv.domain.repository.MeterTypeRepository;
import ru.osh.vkv.domain.repository.UserRepository;

import java.math.BigDecimal;
import java.util.HashSet;

@RequiredArgsConstructor
@Transactional
@Component
public class V3_MeterMigration implements Migration {

    private final MeterRepository meterRepository;
    private final UserRepository userRepository;
    private final MeterTypeRepository meterTypeRepository;

    @Override
    public void migrate() {
        UserEntity user = userRepository.findByUsername("user").orElseThrow();

        meterRepository.save(new MeterEntity(user, new HashSet<>(), "34831341", meterTypeRepository.findByName("ELECTRICITY"), "Address 1", BigDecimal.ZERO, true));
        meterRepository.save(new MeterEntity(user, new HashSet<>(), "130005573", meterTypeRepository.findByName("WATER"), "Address 1", BigDecimal.ZERO, true));
        meterRepository.save(new MeterEntity(user, new HashSet<>(), "1234567", meterTypeRepository.findByName("GAS"), "Address 1", BigDecimal.ZERO, true));
        meterRepository.save(new MeterEntity(user, new HashSet<>(), "PASSPORT_ID-1", meterTypeRepository.findByName("LIFE"), "Address 1", BigDecimal.ZERO, true));
    }
}
