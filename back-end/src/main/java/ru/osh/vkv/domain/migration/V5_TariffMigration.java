package ru.osh.vkv.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.TariffEntity;
import ru.osh.vkv.domain.repository.MeterTypeRepository;
import ru.osh.vkv.domain.repository.TariffRepository;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Transactional
@Component
public class V5_TariffMigration implements Migration {

    private final TariffRepository tariffRepository;
    private final MeterTypeRepository meterTypeRepository;

    @Override
    public void migrate() {
        tariffRepository.save(new TariffEntity(meterTypeRepository.findByName("ELECTRICITY"), BigDecimal.valueOf(5.45)));
        tariffRepository.save(new TariffEntity(meterTypeRepository.findByName("GAS"), BigDecimal.valueOf(2.45)));
        tariffRepository.save(new TariffEntity(meterTypeRepository.findByName("WATER"), BigDecimal.valueOf(15.45)));
        tariffRepository.save(new TariffEntity(meterTypeRepository.findByName("WARM"), BigDecimal.valueOf(7.45)));
        tariffRepository.save(new TariffEntity(meterTypeRepository.findByName("DISTANCE"), BigDecimal.valueOf(51)));
        tariffRepository.save(new TariffEntity(meterTypeRepository.findByName("LIFE"), BigDecimal.valueOf(300000)));
    }
}
