package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.osh.vkv.domain.model.MigrationEntity;

public interface MigrationRepository extends JpaRepository<MigrationEntity, Long> {

    boolean existsByClassName(String className);
}
