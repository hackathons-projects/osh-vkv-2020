package ru.osh.vkv.domain.migration;

/**
 * Conventional naming by order
 */
public interface Migration {
    void migrate();
}
