package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.osh.vkv.domain.model.MeterEntity;
import ru.osh.vkv.domain.model.UserEntity;

import java.util.List;

public interface MeterRepository extends PagingAndSortingRepository<MeterEntity, Long>, JpaRepository<MeterEntity, Long> {

    List<MeterEntity> findAllByUser(UserEntity user);

    List<MeterEntity> findAllByUserAndCurrentAddress(UserEntity user, String currentAddress);

    MeterEntity findByMeterType_NameAndIdentifier(String meterTypeName, String identifier);
}
