package ru.osh.vkv.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.osh.vkv.domain.model.ScoringRecordEntity;

public interface ScoringRecordRepository extends JpaRepository<ScoringRecordEntity, Long> {
}
