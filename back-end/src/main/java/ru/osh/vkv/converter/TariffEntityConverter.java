package ru.osh.vkv.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.TariffEntity;
import ru.osh.vkv.model.TariffDto;

import java.util.Optional;

@Transactional
@Component
public class TariffEntityConverter implements Converter<TariffEntity, TariffDto> {

    @Override
    public TariffDto convert(MappingContext<TariffEntity, TariffDto> mappingContext) {
        TariffDto tariffDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new TariffDto());
        tariffDto.setMeterType(mappingContext.getSource().getMeterType().getName());
        tariffDto.setAmountPerOne(mappingContext.getSource().getAmountPerOne());

        return tariffDto;
    }
}
