package ru.osh.vkv.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.MeterEntity;
import ru.osh.vkv.model.MeterDto;

import java.util.Optional;

@Transactional
@Component
public class MeterEntityConverter implements Converter<MeterEntity, MeterDto> {
    @Override
    public MeterDto convert(MappingContext<MeterEntity, MeterDto> mappingContext) {
        MeterDto meterDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new MeterDto());
        meterDto.setIdentifier(mappingContext.getSource().getIdentifier());
        meterDto.setCurrentAddress(mappingContext.getSource().getCurrentAddress());
        meterDto.setIsEnabled(mappingContext.getSource().getIsEnabled());
        meterDto.setCurrentValue(mappingContext.getSource().getCurrentValue());
        meterDto.setUsername(mappingContext.getSource().getUser().getUsername());
        meterDto.setSubmissionsCount(mappingContext.getSource().getSubmissions().size());
        meterDto.setMeterType(mappingContext.getSource().getMeterType().getName());
        meterDto.setMeterCurrency(mappingContext.getSource().getMeterType().getCurrency());

        return meterDto;
    }
}
