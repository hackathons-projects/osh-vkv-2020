package ru.osh.vkv.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.SubmissionEntity;
import ru.osh.vkv.model.MeterSubmissionDto;

import java.util.Optional;

@Transactional
@Component
public class SubmissionConverter implements Converter<SubmissionEntity, MeterSubmissionDto> {

    @Override
    public MeterSubmissionDto convert(MappingContext<SubmissionEntity, MeterSubmissionDto> mappingContext) {
        MeterSubmissionDto submissionDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new MeterSubmissionDto());
        submissionDto.setAddress(mappingContext.getSource().getAddress());
        submissionDto.setMeterId(mappingContext.getSource().getMeter().getIdentifier());
        submissionDto.setType(mappingContext.getSource().getMeter().getMeterType().getName());
        submissionDto.setUserId(mappingContext.getSource().getUser().getId());
        submissionDto.setPreviousValue(mappingContext.getSource().getPreviousValue());
        submissionDto.setValue(mappingContext.getSource().getValue());
        submissionDto.setCurrency(mappingContext.getSource().getMeter().getMeterType().getCurrency());
        submissionDto.setPayed(mappingContext.getSource().getPayed());

        return submissionDto;
    }
}
