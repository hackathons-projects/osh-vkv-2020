package ru.osh.vkv.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.model.UserDto;

import java.util.Optional;

@Transactional
@Component
public class UserEntityConverter implements Converter<UserEntity, UserDto> {

    @Override
    public UserDto convert(MappingContext<UserEntity, UserDto> mappingContext) {
        UserDto userDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new UserDto());
        userDto.setId(mappingContext.getSource().getId());
        userDto.setUsername(mappingContext.getSource().getUsername());
        userDto.setNickname(mappingContext.getSource().getNickname());
        userDto.setEmail(mappingContext.getSource().getEmail());
        userDto.setRole(mappingContext.getSource().getRole());
        userDto.setPhoneNumber(mappingContext.getSource().getPhoneNumber());
        userDto.setFirstName(mappingContext.getSource().getFirstName());
        userDto.setLastName(mappingContext.getSource().getLastName());
        userDto.setSurname(mappingContext.getSource().getSurname());
        userDto.setInn(mappingContext.getSource().getInn());

        return userDto;
    }
}
