package ru.osh.vkv.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.osh.vkv.model.MeterDto;
import ru.osh.vkv.model.UserDto;
import ru.osh.vkv.model.UserSubmissionsDto;
import ru.osh.vkv.model.request.CreateMeterRequest;
import ru.osh.vkv.model.request.UpdateMeterRequest;
import ru.osh.vkv.service.MeterService;
import ru.osh.vkv.service.UserService;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RolesAllowed("ADMIN")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final MeterService meterService;
    private final UserService userService;

    @PutMapping("/meter")
    public MeterDto updateMeter(@RequestBody UpdateMeterRequest updateMeterRequest) {
        return meterService.updateMeter(updateMeterRequest);
    }

    @PostMapping("/meter")
    public MeterDto createMeter(@RequestBody CreateMeterRequest createMeterRequest) {
        return meterService.createMeter(createMeterRequest);
    }

    @GetMapping("/meter")
    public List<MeterDto> allUserMeters(@RequestParam String username) {
        return meterService.allUserMeters(username);
    }

    @GetMapping("/submissions")
    public UserSubmissionsDto allUserSubmissionsByAddress(@RequestParam String username) {
        return meterService.allUserSubmissions(username);
    }

    @PostMapping("/submission/{id}/complete")
    public void completeSubmission(@PathVariable Long id) {
        meterService.completeSubmission(id);
    }

    @GetMapping("/users")
    public List<UserDto> allUsers() {
        return userService.findAll();
    }
}
