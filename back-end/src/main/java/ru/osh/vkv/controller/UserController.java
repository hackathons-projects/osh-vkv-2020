package ru.osh.vkv.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.osh.vkv.model.UserDto;
import ru.osh.vkv.model.request.SignInRequest;
import ru.osh.vkv.model.request.UserRegistrationRequest;
import ru.osh.vkv.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/signUp")
    public String signUp(@RequestBody UserRegistrationRequest registrationRequest, Principal principal) {
        return userService.signUp(registrationRequest);
    }

    @PostMapping("/signIn")
    public String singIn(@RequestBody SignInRequest signInRequest) {
        return userService.signIn(signInRequest.getUsername(), signInRequest.getPassword());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/whoAmI")
    public ResponseEntity<UserDto> whoAmI(HttpServletRequest request) {
        return ResponseEntity.ok(userService.whoAmI(request));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public ResponseEntity<UserDto> getUserByUsername(@RequestParam(value = "username") String username) {
        return ResponseEntity.ok(userService.search(username));
    }

}
