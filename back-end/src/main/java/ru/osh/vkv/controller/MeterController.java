package ru.osh.vkv.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.osh.vkv.model.MeterDto;
import ru.osh.vkv.model.MeterSubmissionDto;
import ru.osh.vkv.model.UserSubmissionsDto;
import ru.osh.vkv.model.request.SubmitMeterDataRequest;
import ru.osh.vkv.service.MeterService;
import ru.osh.vkv.service.UserService;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Set;

@Slf4j
@RolesAllowed("USER")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/meters")
public class MeterController {

    private final MeterService meterService;
    private final UserService userService;

    @GetMapping("/addresses")
    public Set<String> userAddresses() {
        return meterService.userAddresses(userService.getCurrentUser().getUsername());
    }

    @GetMapping
    public List<MeterDto> allUserMeters() {
        return meterService.allUserMeters(userService.getCurrentUser().getUsername());
    }

    @GetMapping("/byAddress")
    public List<MeterDto> allUserMetersByAddress(@RequestParam String address) {
        return meterService.allUserMetersByAddress(userService.getCurrentUser().getUsername(), address);
    }

    @GetMapping("/submissions")
    public UserSubmissionsDto allUserSubmissionsByAddress() {
        return meterService.allUserSubmissions(userService.getCurrentUser().getUsername());
    }

    @PostMapping
    public MeterSubmissionDto submit(@RequestBody SubmitMeterDataRequest submitMeterDataRequest) {
        return meterService.submitMeterDataRequest(userService.getCurrentUser().getUsername(), submitMeterDataRequest);
    }

    @PostMapping("/requestByMail")
    public void sendReceiptByMail(@RequestParam String email) {
        log.info("Send to email" + email);
    }
}
