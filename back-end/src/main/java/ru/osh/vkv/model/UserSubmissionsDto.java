package ru.osh.vkv.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserSubmissionsDto {

    private Map<String, List<MeterSubmissionDto>> meters = new HashMap<>();

    public void put(String address, MeterSubmissionDto meterSubmissionDto) {

        if (Optional.ofNullable(meters.get(address)).isEmpty()) {
            meters.put(address, new ArrayList<>());
        }

        meters.get(address).add(meterSubmissionDto);
    }
}
