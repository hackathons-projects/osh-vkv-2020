package ru.osh.vkv.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateMeterRequest {

    private String name;

    private BigDecimal currentValue;

    private String identifier;

    private String meterType;

    private String currentAddress;

    private Boolean isEnabled;
}
