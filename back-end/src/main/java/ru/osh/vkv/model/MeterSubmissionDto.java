package ru.osh.vkv.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MeterSubmissionDto {

    private Long userId;

    private String address;

    private String meterId;

    private BigDecimal previousValue;

    private BigDecimal value;

    private String type;

    private String currency;

    private Boolean payed;
}
