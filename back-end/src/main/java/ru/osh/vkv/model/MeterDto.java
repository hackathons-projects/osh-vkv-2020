package ru.osh.vkv.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MeterDto {
    private String username;

    private Integer submissionsCount;

    private BigDecimal currentValue;

    private String identifier;

    private String meterType;

    private String meterCurrency;

    private String currentAddress;

    private Boolean isEnabled;
}
