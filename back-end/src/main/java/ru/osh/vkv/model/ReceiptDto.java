package ru.osh.vkv.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReceiptDto {

    private UserDto userDto;
    private List<MeterDto> meters;
    private List<TariffDto> tariffs;
    private UserSubmissionsDto submissions;

}
