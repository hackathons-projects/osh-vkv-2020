package ru.osh.vkv.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SubmitMeterDataRequest {

    private String identifier;

    private String value;

    private String type;
}
