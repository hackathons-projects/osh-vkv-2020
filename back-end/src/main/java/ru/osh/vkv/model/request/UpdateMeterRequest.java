package ru.osh.vkv.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UpdateMeterRequest {
    private String username;

    private String identifier;

    private String meterType;

    private String currentAddress;

    private BigDecimal currentValue;

    private Boolean isEnabled;
}
