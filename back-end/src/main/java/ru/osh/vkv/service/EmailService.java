package ru.osh.vkv.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.configuration.MailgunConfig;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.TariffRepository;
import ru.osh.vkv.model.*;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class EmailService {

    private final MailgunConfig mailgunConfig;
    private final UserService userService;
    private final MeterService meterService;
    private final TariffRepository tariffRepository;
    private final FileStorageService fileStorageService;
    private final ModelMapper modelMapper;

    public void sendEmail(String username) {
        UserEntity user = userService.findByName(username);
        List<MeterDto> meterDtos = meterService.allUserMeters(username);
        UserSubmissionsDto userSubmissionsDto = meterService.allUserSubmissions(username);
        sendEmail(user, new ReceiptDto(
                modelMapper.map(user, UserDto.class),
                meterDtos,
                tariffRepository.findAll().stream().map(tariffEntity -> modelMapper.map(tariffEntity, TariffDto.class)).collect(Collectors.toList()),
                userSubmissionsDto
        ));
    }

    private void sendEmail(UserEntity user, ReceiptDto receiptDto) {
        try {
            File receipt = fileStorageService.createReceipt(receiptDto);
            HttpResponse<JsonNode> request = Unirest.post(mailgunConfig.getUrl() + "/messages")
                    .basicAuth("api", mailgunConfig.getApiKey())
                    .queryString("from", "noreply@osh-meter.ru")
                    .queryString("to", user.getEmail())
                    .queryString("subject", "Оплатите счет")
                    .queryString("text", "Оплатите счет до отключения услуги...")
                    .field("attachment", Collections.singletonList(receipt))
                    .asJson();
            JsonNode body = request.getBody();
            log.info(body.toString());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}
