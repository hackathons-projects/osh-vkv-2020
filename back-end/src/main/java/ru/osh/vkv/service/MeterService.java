package ru.osh.vkv.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.MeterEntity;
import ru.osh.vkv.domain.model.MeterTypeEntity;
import ru.osh.vkv.domain.model.SubmissionEntity;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.MeterRepository;
import ru.osh.vkv.domain.repository.MeterTypeRepository;
import ru.osh.vkv.domain.repository.SubmissionRepository;
import ru.osh.vkv.exception.CustomException;
import ru.osh.vkv.model.MeterDto;
import ru.osh.vkv.model.MeterSubmissionDto;
import ru.osh.vkv.model.UserSubmissionsDto;
import ru.osh.vkv.model.request.CreateMeterRequest;
import ru.osh.vkv.model.request.SubmitMeterDataRequest;
import ru.osh.vkv.model.request.UpdateMeterRequest;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class MeterService {

    private final UserService userService;
    private final MeterRepository meterRepository;
    private final MeterTypeRepository meterTypeRepository;
    private final SubmissionRepository submissionRepository;
    private final ModelMapper modelMapper;

    public Set<String> userAddresses(String name) {
        UserEntity user = userService.findByName(name);
        return meterRepository.findAllByUser(user).stream().map(MeterEntity::getCurrentAddress).collect(Collectors.toSet());

    }

    public List<MeterDto> allUserMeters(String name) {
        UserEntity user = userService.findByName(name);
        return meterRepository.findAllByUser(user)
                .stream().map(meterEntity -> modelMapper.map(meterEntity, MeterDto.class)).collect(Collectors.toList());
    }

    public List<MeterDto> allUserMetersByAddress(String name, String address) {
        UserEntity user = userService.findByName(name);
        return meterRepository.findAllByUserAndCurrentAddress(user, address)
                .stream().map(meterEntity -> modelMapper.map(meterEntity, MeterDto.class)).collect(Collectors.toList());
    }

    public MeterSubmissionDto submitMeterDataRequest(String name, SubmitMeterDataRequest submitMeterDataRequest) {
        UserEntity user = userService.findByName(name);
        MeterEntity meter = meterRepository.findByMeterType_NameAndIdentifier(submitMeterDataRequest.getType(), submitMeterDataRequest.getIdentifier());
        if (new BigDecimal(submitMeterDataRequest.getValue()).compareTo(meter.getCurrentValue()) < 0) {
            throw new CustomException("Value should not be lower than current value", HttpStatus.NOT_ACCEPTABLE);
        }
        SubmissionEntity saved = submissionRepository.save(
                new SubmissionEntity(user, meter, meter.getCurrentAddress(), meter.getCurrentValue(), new BigDecimal(submitMeterDataRequest.getValue()), false)
        );
        meter.getSubmissions().add(saved);
        meter.setCurrentValue(saved.getValue());
        meterRepository.save(meter);
        return modelMapper.map(saved, MeterSubmissionDto.class);
    }

    public UserSubmissionsDto allUserSubmissions(String name) {
        UserEntity user = userService.findByName(name);
        UserSubmissionsDto result = new UserSubmissionsDto();
        submissionRepository.findAllByUser(user)
                .stream().map(submissionEntity -> modelMapper.map(submissionEntity, MeterSubmissionDto.class))
                .forEach(meterSubmissionDto -> result.put(meterSubmissionDto.getAddress(), meterSubmissionDto));
        return result;
    }

    MeterEntity createMeter(UserEntity user, String identifier, MeterTypeEntity meterType, String currentAddress) {
        return meterRepository.save(new MeterEntity(user, new HashSet<>(), identifier, meterType, currentAddress, BigDecimal.ZERO, false));
    }

    public MeterDto createMeter(CreateMeterRequest createMeterRequest) {
        MeterEntity saved = meterRepository.save(new MeterEntity(
                userService.findByName(createMeterRequest.getName()),
                new HashSet<>(),
                createMeterRequest.getIdentifier(),
                meterTypeRepository.findByName(createMeterRequest.getMeterType()),
                createMeterRequest.getCurrentAddress(),
                createMeterRequest.getCurrentValue(),
                createMeterRequest.getIsEnabled()
        ));
        return modelMapper.map(saved, MeterDto.class);
    }

    public MeterDto updateMeter(UpdateMeterRequest updateMeterRequest) {
        MeterEntity meter = Optional.ofNullable(meterRepository.findByMeterType_NameAndIdentifier(
                updateMeterRequest.getMeterType(), updateMeterRequest.getIdentifier())).orElseThrow();
        if (!meter.getUser().getUsername().equals(updateMeterRequest.getUsername())) {
            UserEntity byName = userService.findByName(updateMeterRequest.getUsername());
            meter.setUser(byName);
        }
        meter.setCurrentValue(updateMeterRequest.getCurrentValue());
        meter.setCurrentAddress(updateMeterRequest.getCurrentAddress());
        meter.setIdentifier(updateMeterRequest.getIdentifier());
        meter.setIsEnabled(updateMeterRequest.getIsEnabled());
        MeterEntity saved = meterRepository.save(meter);

        return modelMapper.map(saved, MeterDto.class);
    }

    public void completeSubmission(Long id) {
        SubmissionEntity submissionEntity = submissionRepository.findOneById(id).orElseThrow();
        submissionEntity.setPayed(true);
        submissionRepository.save(submissionEntity);
    }
}
