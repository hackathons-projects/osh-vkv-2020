package ru.osh.vkv.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.configuration.PaymentConfig;
import ru.osh.vkv.exception.CustomException;
import ru.osh.vkv.model.MeterSubmissionDto;
import ru.osh.vkv.model.ReceiptDto;
import ru.osh.vkv.model.TariffDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private final BaseFont baseFont;

    private final PaymentConfig paymentConfig;

    @Autowired
    public FileStorageService(@Value("${filestorage.location}") String fileStorageLocation,
                              PaymentConfig paymentConfig) {
        this.paymentConfig = paymentConfig;
        this.fileStorageLocation = Paths.get(fileStorageLocation)
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new CustomException("Could not create the directory where the uploaded files will be stored.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        try {
            baseFont = BaseFont.createFont(
                    "src/main/resources/fonts/FreeSans.ttf",
                    "CP1251",
                    BaseFont.EMBEDDED);
        } catch (DocumentException | IOException e) {
            log.error(e.getMessage(), e);
            throw new CustomException("Init pdf file creator", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public File createReceipt(ReceiptDto receiptDto) {
        Map<String, BigDecimal> tariffs = receiptDto.getTariffs().stream().collect(Collectors.toMap(
                TariffDto::getMeterType, TariffDto::getAmountPerOne));
        List<String> qrForRemove = new ArrayList<>();

        try {
            File pdfFile = new File(fileStorageLocation + File.separator + UUID.randomUUID().toString() + ".pdf");
            Font headerFont = new Font(baseFont, 16);
            Font bodyFont = new Font(baseFont, 14);

            final Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

            document.open();
            document.add(new Paragraph("Квитанция оплаты услуг", headerFont));
            document.add(Chunk.NEWLINE);
            document.add(new Paragraph("Пользователь: " + receiptDto.getUserDto().getFirstName() + "  "
                    + receiptDto.getUserDto().getSurname() + " " + receiptDto.getUserDto().getLastName() + " - "
                    + "( " + receiptDto.getUserDto().getPhoneNumber() + ")",
                    bodyFont));
            document.add(Chunk.NEWLINE);

            document.add(new Paragraph("Адреса:", bodyFont));
            document.add(Chunk.NEWLINE);
            for (Map.Entry<String, List<MeterSubmissionDto>> stringListEntry : receiptDto.getSubmissions().getMeters().entrySet()) {
                BigDecimal amount = BigDecimal.ZERO;

                document.add(new Paragraph("Адрес: " + stringListEntry.getKey(), bodyFont));
                document.add(Chunk.NEWLINE);

                for (MeterSubmissionDto submissionDto : stringListEntry.getValue()) {
                    if (!submissionDto.getPayed()) {
                        document.add(new Paragraph("Показания: " + submissionDto.getMeterId() + " - " + submissionDto.getType()
                                + " = " + submissionDto.getValue() + " " + submissionDto.getCurrency(), bodyFont));
                        document.add(Chunk.NEWLINE);
                        document.add(new Paragraph("Разница: " + submissionDto.getValue().subtract(submissionDto.getPreviousValue()) + " " + submissionDto.getCurrency(), bodyFont));
                        document.add(Chunk.NEWLINE);
                        amount = amount.add(
                                submissionDto.getValue().subtract(submissionDto.getPreviousValue()).multiply(tariffs.get(submissionDto.getType()))
                        );
                    }
                }
                if (amount.compareTo(BigDecimal.ZERO) > 0) {
                    String qrFileName = UUID.randomUUID() + ".png";
                    generateQRCodeImage(
                            String.format(paymentConfig.getTemplate(),
                                    paymentConfig.getName(),
                                    paymentConfig.getPersonalAcc(),
                                    paymentConfig.getBankName(),
                                    paymentConfig.getBic(),
                                    paymentConfig.getCorrespAcc(),
                                    paymentConfig.getKpp(),
                                    amount,
                                    BigDecimal.ZERO,
                                    paymentConfig.getPurpose(),
                                    receiptDto.getUserDto().getInn(),
                                    stringListEntry.getKey(),
                                    paymentConfig.getPayerAcc(),
                                    LocalDate.now().toString(),
                                    paymentConfig.getCategory()
                            ), 200, 200, fileStorageLocation + File.separator + qrFileName
                    );

                    document.add(new Paragraph("Сумма: " + amount + " руб.", bodyFont));
                    document.add(Chunk.NEWLINE);

                    Image img = Image.getInstance(fileStorageLocation + File.separator + qrFileName);
                    document.add(img);
                    qrForRemove.add(qrFileName);
                }
            }

            document.close();
            qrForRemove.forEach(this::removeFile);

            return pdfFile;
        } catch (IOException | DocumentException | WriterException e) {
            log.error(e.getMessage(), e);
            throw new CustomException("Pdf file creation", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void removeFile(String filename) {
        try {
            Files.delete(Paths.get(fileStorageLocation + File.separator + filename));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new CustomException("File not found", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
    }

}
