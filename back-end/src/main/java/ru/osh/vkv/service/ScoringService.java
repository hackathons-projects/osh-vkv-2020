package ru.osh.vkv.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.modelmapper.ModelMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.ScoringRecordEntity;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.ScoringRecordRepository;
import ru.osh.vkv.domain.repository.TariffRepository;
import ru.osh.vkv.model.MeterSubmissionDto;
import ru.osh.vkv.model.TariffDto;
import ru.osh.vkv.model.UserDto;
import ru.osh.vkv.model.UserSubmissionsDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class ScoringService {

    private final UserService userService;
    private final MeterService meterService;
    private final ModelMapper modelMapper;
    private final TariffRepository tariffRepository;
    private final ScoringRecordRepository scoringRecordRepository;

    private final double CURRENCY_MULTIPLIER = -0.00001;
    private final Function<BigDecimal, Double> SCORE_FUNCTION = debt -> Math.exp(CURRENCY_MULTIPLIER * debt.doubleValue());

    @Scheduled(cron = "${scoring.cron}")
    private void executeScoring() {
        userService.findAll().forEach(this::scoreUser);
    }

    private void scoreUser(UserDto user) {
        UserSubmissionsDto userSubmissions = meterService.allUserSubmissions(user.getUsername());
        Map<String, BigDecimal> tariffs = tariffRepository.findAll().stream()
                .map(tariffEntity -> modelMapper.map(tariffEntity, TariffDto.class)).collect(Collectors.toMap(
                        TariffDto::getMeterType, TariffDto::getAmountPerOne));
        UserEntity userEntity = userService.findByName(user.getUsername());
        BigDecimal amount = BigDecimal.ZERO;

        for (Map.Entry<String, List<MeterSubmissionDto>> stringListEntry : userSubmissions.getMeters().entrySet()) {
            for (MeterSubmissionDto submissionDto : stringListEntry.getValue()) {
                if (!submissionDto.getPayed()) {
                    amount = amount.add(
                            submissionDto.getValue().subtract(submissionDto.getPreviousValue()).multiply(tariffs.get(submissionDto.getType()))
                    );
                }
            }
        }

        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            scoringRecordRepository.save(new ScoringRecordEntity(userEntity, amount, SCORE_FUNCTION.apply(amount)));
        }

        List<ScoringRecordEntity> records = scoringRecordRepository.findAll();
        if (records.size() > 0) {
            DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
            for (double v : records.stream().map(ScoringRecordEntity::getScore).collect(Collectors.toList())) {
                descriptiveStatistics.addValue(v);
            }
            if (descriptiveStatistics.getMean() < 0.4) {
                log.info("Time to offer microloan");
                // TODO some action!
            }
        }
    }

}
