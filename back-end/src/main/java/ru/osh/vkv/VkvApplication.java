package ru.osh.vkv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan("ru.osh.vkv")
public class VkvApplication {

    public static void main(String[] args) {
        SpringApplication.run(VkvApplication.class, args);
    }

}
