package ru.osh.vkv.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.MeterEntity;
import ru.osh.vkv.domain.model.MeterTypeEntity;
import ru.osh.vkv.domain.model.SubmissionEntity;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.MeterRepository;
import ru.osh.vkv.domain.repository.MeterTypeRepository;
import ru.osh.vkv.domain.repository.SubmissionRepository;
import ru.osh.vkv.model.MeterDto;
import ru.osh.vkv.model.MeterSubmissionDto;
import ru.osh.vkv.model.UserSubmissionsDto;
import ru.osh.vkv.model.request.SubmitMeterDataRequest;
import ru.osh.vkv.model.request.UserRegistrationRequest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional
@SpringBootTest
public class MeterServiceTests extends AbstractServiceTests {

    @Autowired
    private UserService userService;

    @Autowired
    private MeterService meterService;

    @Autowired
    private MeterRepository meterRepository;

    @Autowired
    private MeterTypeRepository meterTypeRepository;

    @Autowired
    private SubmissionRepository submissionRepository;

    @Test
    public void testMobileLogicFlow() {
        UserRegistrationRequest testUserRequest = createTestUser();
        userService.signUp(testUserRequest);
        UserEntity testUser = userService.findByName("test");
        MeterTypeEntity anyType = meterTypeRepository.findAll().stream().findAny().orElseThrow();
        MeterEntity testMeter1 = meterService.createMeter(testUser, "1234", anyType, "Address test");
        MeterEntity testMeter2 = meterService.createMeter(testUser, "12345", anyType, "Address test");
        MeterEntity testMeter3 = meterService.createMeter(testUser, "fdfdfdfd", anyType, "Address test 2");

        Set<String> addresses = meterService.userAddresses("test");
        Assertions.assertEquals(2, addresses.size());
        Assertions.assertTrue(addresses.contains("Address test"));
        Assertions.assertTrue(addresses.contains("Address test 2"));

        List<MeterDto> res = meterService.allUserMeters("test");
        Set<String> collectedMeters = res.stream().map(MeterDto::getIdentifier).collect(Collectors.toSet());
        Assertions.assertEquals(3, res.size());
        Assertions.assertTrue(collectedMeters.contains(testMeter1.getIdentifier()));
        Assertions.assertTrue(collectedMeters.contains(testMeter2.getIdentifier()));
        Assertions.assertTrue(collectedMeters.contains(testMeter3.getIdentifier()));


        MeterDto first = res.stream().filter(meterDto -> meterDto.getIdentifier().equals(testMeter1.getIdentifier())).findFirst().orElseThrow();
        Assertions.assertEquals(testMeter1.getIdentifier(), first.getIdentifier());
        Assertions.assertEquals(testMeter1.getCurrentAddress(), first.getCurrentAddress());
        Assertions.assertEquals(testMeter1.getCurrentValue(), first.getCurrentValue());
        Assertions.assertEquals(testMeter1.getIsEnabled(), first.getIsEnabled());
        Assertions.assertEquals(testMeter1.getMeterType().getName(), first.getMeterType());
        Assertions.assertEquals(testMeter1.getUser().getUsername(), first.getUsername());
        Assertions.assertEquals(testMeter1.getSubmissions().size(), first.getSubmissionsCount());

        List<MeterDto> userSubmissionsDtos = meterService.allUserMetersByAddress("test", "Address test");
        Set<String> collectedMeters2 = userSubmissionsDtos.stream().map(MeterDto::getIdentifier).collect(Collectors.toSet());
        Assertions.assertEquals(2, userSubmissionsDtos.size());
        Assertions.assertTrue(collectedMeters2.contains(testMeter1.getIdentifier()));
        Assertions.assertTrue(collectedMeters2.contains(testMeter2.getIdentifier()));
        Assertions.assertFalse(collectedMeters2.contains(testMeter3.getIdentifier()));

        MeterSubmissionDto sub1 = meterService.submitMeterDataRequest("test", new SubmitMeterDataRequest("12345", "100.0", anyType.getName()));
        MeterEntity byId = meterRepository.findById(testMeter2.getId()).orElseThrow();
        Set<SubmissionEntity> submissions = byId.getSubmissions();
        Assertions.assertEquals(1, submissions.size());
        SubmissionEntity next = submissions.iterator().next();
        Assertions.assertEquals("Address test", next.getAddress());
        Assertions.assertEquals(testMeter2.getId(), next.getMeter().getId());
        Assertions.assertEquals("test", next.getUser().getUsername());
        Assertions.assertEquals(new BigDecimal("100.0"), next.getValue());

        Assertions.assertEquals(next.getAddress(), sub1.getAddress());
        Assertions.assertEquals(next.getMeter().getMeterType().getName(), sub1.getType());
        Assertions.assertEquals(next.getMeter().getIdentifier(), sub1.getMeterId());
        Assertions.assertEquals(next.getUser().getId(), sub1.getUserId());
        Assertions.assertEquals(next.getValue(), sub1.getValue());

        MeterSubmissionDto sub2 = meterService.submitMeterDataRequest("test", new SubmitMeterDataRequest("12345", "100.0", anyType.getName()));
        MeterSubmissionDto sub3 = meterService.submitMeterDataRequest("test", new SubmitMeterDataRequest("fdfdfdfd", "100.0", anyType.getName()));
        UserSubmissionsDto map = meterService.allUserSubmissions("test");
        Assertions.assertEquals(2, map.getMeters().keySet().size());
        Assertions.assertEquals(1, map.getMeters().get("Address test 2").size());
        Assertions.assertEquals(2, map.getMeters().get("Address test").size());
        Assertions.assertTrue(map.getMeters().get("Address test").contains(sub1));
        Assertions.assertTrue(map.getMeters().get("Address test").contains(sub2));
        Assertions.assertTrue(map.getMeters().get("Address test 2").contains(sub3));

    }

}
