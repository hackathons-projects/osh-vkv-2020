package ru.osh.vkv.service;

import ru.osh.vkv.model.request.UserRegistrationRequest;

abstract class AbstractServiceTests {

    protected UserRegistrationRequest createTestUser() {
        UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setUsername("test");
        userRegistrationRequest.setPassword("test");
        userRegistrationRequest.setNickname("test");
        userRegistrationRequest.setPhoneNumber("+7-000-000-00-00");
        userRegistrationRequest.setInn("123");
        return userRegistrationRequest;
    }
}
