package ru.osh.vkv.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import ru.osh.vkv.domain.model.UserEntity;
import ru.osh.vkv.domain.repository.UserRepository;

@Transactional
@SpringBootTest
public class EmailServiceTests extends AbstractServiceTests {

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserRepository userRepository;

    @Value("${email.send.to}")
    private String email;

    //@Disabled
    @Test
    public void sendTestEmail() {
        UserEntity user = userRepository.findByUsername("user").orElseThrow();
        user.setEmail(email);
        userRepository.save(user);

        emailService.sendEmail("user");
    }
}
