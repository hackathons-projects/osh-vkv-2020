//
//  oshApp.swift
//  osh
//
//  Created by Alexey Khorikov on 23.10.2020.
//

import SwiftUI

@main
struct oshApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
