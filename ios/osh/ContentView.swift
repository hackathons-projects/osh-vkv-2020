//
//  ContentView.swift
//  osh
//
//  Created by Alexey Khorikov on 23.10.2020.
//

import SwiftUI

struct ContentView: View {
    
    @State var image: UIImage?
    @State private var showSheet: Bool = false
    @State private var showImagePicker: Bool = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    
    var body: some View {
//        NavigationView {
//            VStack {
//                Image(uiImage: image ?? UIImage(systemName: "circle")!).resizable().frame(width: 300, height: 300)
//                Button("Choose Picture") {
//                    showImagePicker = true
//                }.padding()
//                .actionSheet(isPresented: self.$showSheet) {
//                    ActionSheet(title: Text("Select photo"), message: Text("Choose"), buttons: [.default(Text("Photo Library")) {
//                        self.showImagePicker = true
//                    }, .default(Text("Camera")){
//                        self.showImagePicker = true
//                    }, .cancel()])
//                }
//
//            }.navigationTitle("DEMO")
//        }.sheet(isPresented: self.$showImagePicker) {
//            ImagePicker(isShowen: self.$showImagePicker, image: self.$image, sourceType: self.sourceType)
//        }
//        ZStack {
//            VStack {
//                RoundedRectangle(cornerRadius: 20).frame(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height).background(Color.white).opacity(0.5)
//            }
//            CameraViewController().edgesIgnoringSafeArea(.top).overlay( RoundedRectangle(cornerRadius: 20).strokeBorder(style: StrokeStyle( lineWidth: 3, dash: [180, 140], dashPhase: 244)).foregroundColor(Color(UIColor.white)).frame(width: UIScreen.main.bounds.size.width-80, height: UIScreen.main.bounds.size.width-80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)).overlay(RoundedRectangle(cornerRadius: 20).stroke(lineWidth: 1.0).frame(width: UIScreen.main.bounds.size.width-80, height: UIScreen.main.bounds.size.width-80)).foregroundColor(Color("BorderCapture"))
//                .mask(
//                ZStack {
//                    RoundedRectangle(cornerRadius: 40).frame(width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.width-90).background(Color.white).opacity(0.5)
//                    Rectangle().frame(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height).background(Color.white).opacity(0.5)
//                }
//        )
//
//        }
        MainView()
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
