//
//  ListView.swift
//  osh
//
//  Created by Alexey Khorikov on 25.10.2020.
//

import SwiftUI

struct ListView: View {
    @State var listItems = [Item(id: 1, img: "Home", place: "Квартира", adress: "Москва, ул. Донская, д. 8 стр. 1, кв. 73"), Item(id: 2, img: "Shape", place: "Гараж", adress: "Москва, Боровское шоссе, 38, к. 1"), Item(id: 3, img: "gatage", place: "Загородний дом", adress: "Москва, поселение Первомайское, квартал № 190"), Item(id: 4, img: "Квартира", place: "Загородний дом", adress: "Нижний Новгород, ул. Фучика, д. 5, кв. 18 ")]
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
            VStack(alignment: .leading) {
                Rectangle().frame(width: 35, height: 2, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Rectangle().frame(width: 35, height: 2, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Rectangle().frame(width: 35, height: 2, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }.padding().padding(.top, 45)
                List {
                        ForEach(listItems /) {
                            
                        }
                }
                    
                Spacer()
                
            }
            
            Spacer()
        }.background(Color("background")).edgesIgnoringSafeArea(.all)
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
