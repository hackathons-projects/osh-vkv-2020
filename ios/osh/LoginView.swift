//
//  LoginView.swift
//  osh
//
//  Created by Alexey Khorikov on 24.10.2020.
//

import SwiftUI

struct LoginView: View {
    
    @State var password: String = ""
    @State var email: String = ""
    
    var body: some View {
        VStack(alignment: .center) {
            ZStack {
                Image("pattern2").resizable().frame(width: UIScreen.main.bounds.size.width, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).aspectRatio(contentMode: .fill)
                Wave(graphWidth: 1.7, amplitude: 0.4).foregroundColor(.white).frame(width: UIScreen.main.bounds.size.width, height: 360).offset(x: -490, y: 40).rotationEffect(.init(degrees: 50))
                Wave(graphWidth: 1.5, amplitude: 0.4).foregroundColor(.white).frame(width: UIScreen.main.bounds.size.width, height: 300).offset(x: -360, y: 60).rotationEffect(.init(degrees: 65))
                Text("Добро пожаловать!").font(.title).offset(y: 100)
            }.padding(.top, -100).frame(width: UIScreen.main.bounds.size.width, height: 300, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
          Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
            Image("google").resizable().aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/).frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Text("Продолжить через Google").font(.subheadline).foregroundColor(.black)
          }).frame(width: UIScreen.main.bounds.size.width - 60, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).font(.title3).padding().overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color("border"), lineWidth: 3)
            )
            Text("Войти по email").fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).textCase(/*@START_MENU_TOKEN@*/.uppercase/*@END_MENU_TOKEN@*/).font(.body).padding(30)
            TextField("Введите Email", text: $email).textFieldStyle(RoundedBorderTextFieldStyle()).padding()
            SecureField("Введите пароль", text: $password).textFieldStyle(RoundedBorderTextFieldStyle()).padding()
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                HStack {
                    Text("Войти")
                    Image("Arrow").resizable().aspectRatio(contentMode: .fit).frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                }
            }
            ) .frame(width: UIScreen.main.bounds.size.width - 60, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).font(.title3).padding().background(Color("mainColor")).foregroundColor(.white).cornerRadius(10)
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                HStack {
                    Text("Забыли пароль?")
                }
            }
           
            ).foregroundColor(.black).padding()
            Spacer()
            HStack {
                Text("У вас не аккаунта?")
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    HStack {
                        Text("Зарегистрироваться").textCase(/*@START_MENU_TOKEN@*/.uppercase/*@END_MENU_TOKEN@*/).foregroundColor(Color("mainColor"))
                    }
                }
                ).foregroundColor(.black)
            }
           Spacer()
        }.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

struct CustomShape: Shape {
    let radius: CGFloat
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let tl = CGPoint(x: rect.minX, y: rect.minY)
        let tr = CGPoint(x: rect.maxX, y: rect.minY)
        let brs = CGPoint(x: rect.maxX, y: rect.maxY - radius)
        let brc = CGPoint(x: rect.maxX + radius, y: rect.maxY + radius)
        let bls = CGPoint(x: rect.minX + radius, y: rect.maxY)
        let blc = CGPoint(x: rect.minX + radius, y: rect.maxY - radius)
        
        path.move(to: tl)
        path.addLine(to: tr)
        path.addLine(to: brs)
        path.addRelativeArc(center: brc, radius: radius,
          startAngle: Angle.degrees(0), delta: Angle.degrees(90))
        path.addLine(to: bls)
        path.addRelativeArc(center: blc, radius: radius,
          startAngle: Angle.degrees(90), delta: Angle.degrees(90))
    
        
        return path
    }
}


struct Wave: Shape {
let graphWidth: CGFloat
let amplitude: CGFloat
func path(in rect: CGRect) -> Path {
let width = UIScreen.main.bounds.size.width
    let height = rect.height
let origin = CGPoint(x: 0, y: height * 0.50)
var path = Path()
            path.move(to: origin)
//var endY: CGFloat = 0.0
let step = 5.0
for angle in stride(from: step, through: Double(width) * (step * step), by: step) {
let x = origin.x + CGFloat(angle/360.0) * width * graphWidth
let y = origin.y - CGFloat(sin(angle/180.0 * Double.pi)) * height * amplitude
                path.addLine(to: CGPoint(x: x, y: y))
//                endY = y
            }
//            path.addLine(to: CGPoint(x: width, y: endY))
            path.addLine(to: CGPoint(x: width, y: height))
            path.addLine(to: CGPoint(x: 0, y: height))
            path.addLine(to: CGPoint(x: 0, y: origin.y))
return path
    }
}
