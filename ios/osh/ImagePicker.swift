//
//  ImagePicker.swift
//  osh
//
//  Created by Alexey Khorikov on 23.10.2020.
//

import Foundation
import SwiftUI

class ImagePickerCoordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @Binding var isShowen: Bool
    @Binding var image: UIImage?
    
    init(isShowen: Binding<Bool>, image: Binding<UIImage?>) {
        _isShowen = isShowen
        _image = image
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let uiImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = uiImage
        }
        
        isShowen = false
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isShowen = false
    }
}

struct ImagePicker: UIViewControllerRepresentable {
    
    typealias UIViewControllerType = UIImagePickerController
    typealias Coordinator = ImagePickerCoordinator
    @Binding var isShowen: Bool
    @Binding var image: UIImage?
    var sourceType: UIImagePickerController.SourceType = .camera
    
    func updateUIViewController(_ uiViewControoller: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePicker>) {
        
    }
    
    func makeCoordinator() -> ImagePickerCoordinator {
        return ImagePickerCoordinator(isShowen: $isShowen, image: $image)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) ->
    UIImagePickerController {
        let picker = UIImagePickerController()
        picker.sourceType = sourceType
        picker.delegate = context.coordinator
        return picker
    }
}
