//
//  ItemView.swift
//  osh
//
//  Created by Alexey Khorikov on 25.10.2020.
//

import SwiftUI

struct ItemView: View {
    @Binding var arr: []
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                ZStack(alignment: .leading) {
//                    RoundedRectangle(cornerRadius: 20).fill(Color("orange")).frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Image("Home").resizable().aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/).padding()
                    
                }.background(Color("orange")).frame(width: 60, height: 60, alignment: .leading).cornerRadius(10)
                Text("Квартира").font(.largeTitle).padding(.horizontal)
                Spacer()
            }.padding(.horizontal)
            
            Rectangle().fill(Color("line")).frame(width: UIScreen.main.bounds.size.width - 60, height: 2, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).padding(.top, 5)
            HStack {
                Button(action: {}, label: {
                    Image("pin").resizable().aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/).frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).padding(5)
                }).overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color("border"), lineWidth: 2)
                ).padding(.horizontal)
                Text("Москва, ул. Донская, д. 8 стр. 1, кв. 73 ").font(.subheadline).frame(width: 200, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).foregroundColor(Color("textColor"))
                Spacer()
                Image("ArrowBlack").resizable().aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/).frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).padding(.horizontal)
            }
            
        }.padding().frame(width: UIScreen.main.bounds.size.width - 60, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.white), lineWidth: 2)
        )
    }
}

//struct ItemView_Previews: PreviewProvider {
//    static var previews: some View {
//        ItemView()
//    }
}
