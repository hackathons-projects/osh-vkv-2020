//
//  MainView.swift
//  osh
//
//  Created by Alexey Khorikov on 24.10.2020.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        ZStack {
            HStack{
                Image("pattern").resizable().aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/).frame(width: UIScreen.main.bounds.size.width/3, height: UIScreen.main.bounds.size.height, alignment: .leading)
                Spacer()
                
            }
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    Image("logo").resizable().aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/).frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    VStack(alignment: .leading) {
                        Text("Открытие").font(.title).bold().textCase(.uppercase)
                        Text("Банк").font(.caption).padding(.top, 1)
                    }.padding(.top, 22)
                    
                }.padding()
                Spacer()
                VStack {
                    HStack {
                        Image("logoApp").resizable().aspectRatio(contentMode: .fit).frame(width: 60, height: 60).padding(.leading, 20)
                        
                    }
                    HStack {
                        Spacer()
                        Text("ОСчётчик").font(.largeTitle).fontWeight(.heavy)
                    }.padding(.trailing, 50)
                }
                
                HStack {
                    Spacer()
                    Text("Учет израсходованной воды, электроэнергии, газа и других ресурсов.").font(.custom("GraphikLC-Regular-Desktop", size: 16)).frame(width: UIScreen.main.bounds.size.width/2, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).foregroundColor(Color("desc"))
                }.padding(.trailing, 30)
                Spacer()
                VStack {
                    HStack {
                        Spacer()
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            HStack {
                                Text("Войти")
                                Image("Arrow").resizable().aspectRatio(contentMode: .fit).frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            }
                        }
                        ) .frame(width: UIScreen.main.bounds.size.width / 2, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).font(.title3).padding().background(Color("mainColor")).foregroundColor(.white).cornerRadius(10)
                    }.padding(.horizontal)
                    HStack {
                        Spacer()
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("Зарегистрироваться").font(.subheadline).foregroundColor(.black)
                        })
                    }.padding(.trailing, 60).padding(.top, 10)
                }.padding(.top, 80)
                
               
                Spacer()
                
            }.padding(.bottom, 70)
            
        }.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
