//
//  ItemModel.swift
//  osh
//
//  Created by Alexey Khorikov on 25.10.2020.
//

import Foundation


struct Item: Hashable, Codable, Identifiable {
    var id: Int
    var img: String
    var place: String
    var adress: String
}
