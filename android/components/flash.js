import Svg, {
  Circle,
  Ellipse,
  G,
  Text,
  TSpan,
  TextPath,
  Path,
  Polygon,
  Polyline,
  Line,
  Rect,
  Use,
  Image,
  Symbol,
  Defs,
  LinearGradient,
  RadialGradient,
  Stop,
  ClipPath,
  Pattern,
  Mask,
} from 'react-native-svg';
import * as React from 'react';
import { View, StyleSheet, } from 'react-native';

export  default class FlashBtn extends React.Component{
  const =  styles = StyleSheet.create({});
  render(){
    return(
    <View>

<svg width="20" height="33" viewBox="0 0 20 33" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M9.03507 18.3691L4.61917 31.8283C4.56508 31.9932 4.6543 32.1708 4.81845 32.2252C4.93855 32.2649 5.07068 32.2281 5.1533 32.132L19.2385 15.7395C19.5208 15.411 19.4844 14.9149 19.1573 14.6314C19.0153 14.5082 18.8339 14.4405 18.6462 14.4405H10.9649L15.3808 0.981261C15.4349 0.816406 15.3457 0.638727 15.1815 0.584405C15.0614 0.544657 14.9293 0.581418 14.8467 0.677571L0.761472 17.07C0.479188 17.3985 0.515528 17.8947 0.842638 18.1782C0.984705 18.3013 1.16611 18.3691 1.35376 18.3691H9.03507Z" fill="white"/>
</svg>


</View>
    );
  }



}