import Svg, {
  Circle,
  Ellipse,
  G,
  Text,
  TSpan,
  TextPath,
  Path,
  Polygon,
  Polyline,
  Line,
  Rect,
  Use,
  Image,
  Symbol,
  Defs,
  LinearGradient,
  RadialGradient,
  Stop,
  ClipPath,
  Pattern,
  Mask,
} from 'react-native-svg';
import * as React from 'react';
import { View, StyleSheet} from 'react-native';
export  default class FotoBtn extends React.Component {
  //const styles =  StyleSheet.create({});
  render(){
    return(
    <View>
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="40" cy="40" r="40" fill="#DB002C"/>
<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="16" cy="16" r="11" fill="white"/>
<circle cx="10" cy="8" r="5" fill="#DB002C"/>
<circle cx="16" cy="16" r="15" stroke="white" stroke-width="2"/>
</svg>


</svg>



      </View>
    )
  }
};
