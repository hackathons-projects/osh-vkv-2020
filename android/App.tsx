import React from  'react';
import {Camera} from 'expo-camera';
import {Text, View, StyleSheet} from 'react-native';
import FlashBtn from './components/flash';
import FotoBtn from './components/FotoBTn';
export default  class CameraScreen extends React.Component{
 render(){
   return(
     <Camera>
     <View style={{ blur: 0.65, backgroundColor:"rgba(0, 0, 0, 0.85)",  width: "100%",  height:"100%" }}>

     <Text style={ styles.closeBtn }>X</Text>
    <Text style={ styles.title }>Сфорографируйте счётчик</Text>
     <View  style={styles.cropPoint}>
      
     </View>
     
     </View>
     <View style={styles.buttomGroup}>
    
     <FlashBtn />
     <FotoBtn />
     
     </View>
     
     </Camera>
   )

 }};

const styles = StyleSheet.create({
  cropPoint:{
    position: "absolute",
    width: "312px",
    height: "336px",
        left: "31px",
        top: "278px",

  },
  title:{

      position: "absolute",
      width: "266px",
      height: "70px",
      left: "55px",
      top: "142px",

      fontFamily: "Graphik LCG",
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "28px",
      lineHeight: "35px",

      /* or 125% */
      textAlign: "center",

      color: "#FFFFFF",

  },
  buttomGroup:{
            
        position: "fixed",
        width: "100%",
        height: "80px",
        bottom: "67px",
        display:"flex",

  },
  closeBtn:{
    /* блюр */

  position: "absolute",
  left: "17px",
  top: "67px",
  color: "#FFF",

  filter: "blur(15px)",


/* UI Bars / Top Bars / Light / Top Bar */



  },



 })
